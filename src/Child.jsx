import { useState } from "react";

const Child = (props) => {
  const [num, changeNum] = useState(0);
  const handleClick = () => {
    changeNum(num + 2);
    props.change(num);
  };
  return (
    <div>
      <h4>{num}</h4>
      <button
        onClick={() => {
          handleClick();
        }}
      >
        Click Me
      </button>
    </div>
  );
};
export default Child;
