import { useState } from "react";
import Child from "./Child";
function App() {
  const [num, changeNum] = useState(0);
  const changeHandler = (data) => {
    changeNum(data);
  };
  return (
    <div>
      <h4>{num}</h4>
      <Child change={changeHandler} />
    </div>
  );
}

export default App;
